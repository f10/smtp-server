#!/bin/sh

######### stuff for downloading maven build system ###########
MAVEN_HOME=./apache-maven-3.0.5
MAVEN_REPOS=./.m2
MVN=$MAVEN_HOME/bin/mvn

FINAL_JAR=./target/smtp-server-0.0.1-SNAPSHOT-jar-with-dependencies.jar

if [ ! -f $MVN ]
then
	echo "Local maven not installed. Fetching binaries from mirror ftp://ftp.fu-berlin.de/unix/www/apache/maven/maven-3/3.0.5/binaries/apache-maven-3.0.5-bin.tar.gz."
	wget ftp://ftp.fu-berlin.de/unix/www/apache/maven/maven-3/3.0.5/binaries/apache-maven-3.0.5-bin.tar.gz
	echo "unpacking."
	gunzip < ./apache-maven-3.0.5-bin.tar.gz | tar xvf -
	echo "done."
fi


#############################################################################
# Change the following two variables
#############################################################################

# compile your code, if necessary
# leave this blank if you do not need to compile code
COMPILECMD="$MVN clean install -Dmaven.repo.local=$MAVEN_REPOS"

# start your gateway (listening on port 12890!)
#RUNCMD="gateway.pl --port 12890"

RUNCMD="java -jar $FINAL_JAR"

#############################################################################
if [ ! -f $FINAL_JAR ]
then
  echo "no binary found... starting build system."
  $COMPILECMD
fi
# you can use for example ./run.sh -p 1234 -b 192.168.0.4
# type ./run.sh -h for help
$RUNCMD $@
