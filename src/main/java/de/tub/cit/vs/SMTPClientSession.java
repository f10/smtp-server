package de.tub.cit.vs;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

/**
 * The Class SMTPClientSession represents a current session with a SMTP client.
 * 
 * author florianbeckmann@gmx.net
 */
public class SMTPClientSession {

  /**
   * The Enum ClientState represents a list of states a smtp client can have.
   */
  public enum ClientState {

    CONNECTED, SENTWELCOME, MAILFROMRECEIVED, RCPTTORECEIVED, DATASENT, MESSAGESENT, QUITSENT, HELPSENT
  }

  private final static String TERMINATION_STRING = "\r\n.\r\n";

  /** The time this session was created. */
  private long timeCreated;

  /** The client's current state. */
  private ClientState clientState;

  /** The client's previous state. */
  private ClientState previousClientState;

  /** The response for the client. */
  private String response;

  /** The mail from value sent with a MAIL FROM: command. */
  private String mailFrom;

  /** The recipient valie sent with a RCPT TO: command. */
  private String recipient;

  /** The transferred mails actual content. */
  private String mailContent;

  /**
   * Instantiates a new SMTP client session.
   */
  public SMTPClientSession() {
    // instead of import java.util.Random;
    // Random x = new Random();
    // x.nextInt(999+1)
    // we use the current timestamp
    timeCreated = System.currentTimeMillis();
  }

  /**
   * Gets the time created.
   * 
   * @return the time created
   */
  public long getTimeCreated() {
    return timeCreated;
  }

  /**
   * Sets the time created.
   * 
   * @param timeCreated
   *          the new time created
   */
  public void setTimeCreated(long timeCreated) {
    this.timeCreated = timeCreated;
  }

  /**
   * Gets the client state.
   * 
   * @return the client state
   */
  public ClientState getClientState() {
    return clientState;
  }

  /**
   * Sets the client state.
   * 
   * @param clientState
   *          the new client state
   */
  public void setClientState(ClientState clientState) {
    setPreviousClientState(this.clientState);
    this.clientState = clientState;

  }

  /**
   * Gets the previous client state.
   * 
   * @return the previous client state
   */
  public ClientState getPreviousClientState() {
    return previousClientState;
  }

  /**
   * Sets the previous client state.
   * 
   * @param previousClientState
   *          the new previous client state
   */
  private void setPreviousClientState(ClientState previousClientState) {
    this.previousClientState = previousClientState;
  }

  /**
   * Gets the response.
   * 
   * @return the response
   */
  public String getResponse() {
    return response;
  }

  /**
   * Sets the response.
   * 
   * @param response
   *          the new response
   */
  public void setResponse(String response) {
    this.response = response;
  }

  /**
   * Gets the mail from.
   * 
   * @return the mail from
   */
  public String getMailFrom() {
    return mailFrom;
  }

  /**
   * Sets the mail from.
   * 
   * @param mailFrom
   *          the new mail from
   */
  public void setMailFrom(String mailFrom) {
    this.mailFrom = mailFrom.trim();
  }

  /**
   * Gets the recipient.
   * 
   * @return the recipient
   */
  public String getRecipient() {
    return recipient;
  }

  /**
   * Sets the recipient.
   * 
   * @param recipient
   *          the new recipient
   */
  public void setRecipient(String recipient) {
    this.recipient = recipient.trim();
  }

  /**
   * Gets the mail content.
   * 
   * @return the mail content
   */
  public String getMailContent() {
    return mailContent;
  }

  /**
   * Sets the mail content.
   * 
   * @param mailContent
   *          the new mail content
   */
  public void setMailContent(String mailContent) {
    
    if (mailContent.endsWith(TERMINATION_STRING)) {

      mailContent = mailContent.substring(0, mailContent.lastIndexOf(TERMINATION_STRING));
    }

    this.mailContent = mailContent;
  }

  /**
   * Persist message.
   * 
   * @throws IOException
   *           Signals that an I/O exception has occurred.
   */
  public void persistMessage() throws IOException {
    Path dirPath = FileSystems.getDefault().getPath(".", getRecipient());
    Files.createDirectories(dirPath);

    Path filePath = FileSystems.getDefault().getPath("." + File.separator + getRecipient(), getMailFrom() + "_" + getTimeCreated());
    Files.write(filePath, mailContent.getBytes(), StandardOpenOption.CREATE);
  }

}
