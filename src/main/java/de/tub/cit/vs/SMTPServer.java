package de.tub.cit.vs;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.tub.cit.vs.SMTPClientSession.ClientState;

/**
 * This class implements a simple smtp server with java nio.
 * 
 * @author Daniel Baumgart - daniel.baumgart@mailbox.tu-berlin.de
 * @author Florian Beckmann - florianbeckmann@gmx.net
 * @author Jonas Peeck - jonas.peeck@googlemail.com
 */
public class SMTPServer {

  /** The Constant US_ASCII. */
  public static final String US_ASCII = "US-ASCII";

  /** The Constant DEFAULT_HOST. */
  public static final String DEFAULT_HOST = "localhost";

  /** The Constant SMTP_DEFAULT_PORT. */
  public static final int SMTP_DEFAULT_PORT = 6332;

  /** The host's ip address. */
  public String host = DEFAULT_HOST;

  /** The smtp port. */
  public int smtpPort = SMTP_DEFAULT_PORT;

  /**
   * The enum ServerResponse encapsulates the server response strings.
   */
  private enum ServerResponse {

    SERVICE_READY("220 service ready"), OK("250 OK"), START_MAIL_INPUT("354 start mail input"), CLOSING_CHANNEL("221 closing channel"), HELP(
            "214 RTFM or rfc5321"), TMP_ERROR("400");

    /** The response string. */
    private String response;

    /**
     * Instantiates a new server response.
     * 
     * @param response
     *          the response
     */
    private ServerResponse(String response) {
      this.response = response;
    }

    /**
     * Gets the response.
     * 
     * @return the response as string.
     */
    public String getResponse() {
      return response;
    }

  }

  /**
   * The enum ClientRequest encapsulates the client request strings.
   */
  private enum ClientRequest {

    HELP("HELP"), HELO("HELO"), MAIL_FROM("MAIL FROM:"), RCPT_TO("RCPT TO:"), DATA("DATA"), QUIT("QUIT");

    /** The client's request as string. */
    private String request;

    /**
     * Instantiates a new client request.
     * 
     * @param request
     *          the request
     */
    ClientRequest(String request) {
      this.request = request;
    }

    /**
     * Gets the request.
     * 
     * @return the request
     */
    public String getRequest() {
      return request;
    }

    /**
     * To string.
     * 
     * @return the string
     */
    public String toString() {
      return getRequest();
    }

  }

  /** The channelClientSessions maps client socketChannels to SMTPClientSession. */
  private Map<SocketChannel, SMTPClientSession> channelClientSessions = new HashMap<SocketChannel, SMTPClientSession>();

  /** The slf4j logger. */
  private static final Logger logger = LoggerFactory.getLogger(SMTPServer.class);

  /** The selector. */
  private Selector selector;

  /** The server socket channel. */
  private ServerSocketChannel serverSocketChannel;

  /**
   * The main method starts the smtp server.
   * 
   * @param args
   *          the program arguments
   */
  public static void main(String[] args) {
    logger.info("Starting SMTP server.");

    SMTPServer smtpServer = new SMTPServer();

    try {
      smtpServer.start();
    } catch (IOException e) {
      logger.error("main()", e);

    }

    logger.info("Good bye.");

  }

  /**
   * Start. This method opens required server socket and handles the client's
   * requests.
   * 
   * @throws IOException
   *           Signals that an I/O exception has occurred.
   */
  public void start() throws IOException {

    openSelectorAndServerSocket();
    handleRequests();
  }

  /**
   * Open selector and server socket.
   * 
   * @throws IOException
   *           Signals that an I/O exception has occurred.
   */
  private void openSelectorAndServerSocket() throws IOException {
    selector = Selector.open();
    serverSocketChannel = ServerSocketChannel.open();
    serverSocketChannel.configureBlocking(false);
    serverSocketChannel.socket().bind(new InetSocketAddress(host, smtpPort));
    serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
    logger.info("Server socket opened on " + host + ":" + smtpPort);
  }

  /**
   * Handles the clients' requests.
   */
  private void handleRequests() {

    while (true) {

      int noOfChannelKeys = 0;

      try {
        noOfChannelKeys = selector.select();
      } catch (IOException e) {
        logger.error("processSMTPRequest()", e);
        // better throw RuntimeException here and quit?
        noOfChannelKeys = 0;
      }

      if (noOfChannelKeys == 0) {
        continue;
      }

      // get selected keys
      Set<SelectionKey> selectedKeys = selector.selectedKeys();
      Iterator<SelectionKey> keyIterator = selectedKeys.iterator();

      // iterate over keys
      while (keyIterator.hasNext()) {

        SelectionKey key = keyIterator.next();

        if (key.isAcceptable()) {

          // a connection was accepted by a ServerSocketChannel.
          ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
          SocketChannel clientSocketChannel;

          try {
            // try to setup connection to client
            clientSocketChannel = serverSocketChannel.accept();
            logger.info("Client connection from " + getHostPortString(clientSocketChannel) + "] accepted on "
                    + clientSocketChannel.socket().getLocalAddress().getHostAddress() + ":" + clientSocketChannel.socket().getLocalPort() + "]");
            clientSocketChannel.configureBlocking(false);
            clientSocketChannel.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);

            // Create new client session
            SMTPClientSession smtpClientSession = new SMTPClientSession();
            smtpClientSession.setClientState(ClientState.CONNECTED);
            smtpClientSession.setResponse(ServerResponse.SERVICE_READY.getResponse());

            // key.attach(smtpClientSession); // RETRIEVING ATTACHED
            // OBJECT LATER DID NOT WORK->USE HASHMAP
            channelClientSessions.put(clientSocketChannel, smtpClientSession);

            logger.info("Client socket channel registered for r/w.");
          } catch (IOException e) {
            logger.error("Couldn't register client socket channel:");
            logger.error("processSMTPRequest()", e);
          }

        } else if (key.isConnectable()) {

          // a connection was established with a remote server.
          // we don't do anything important here
          SocketChannel clientSocketChannel = (SocketChannel) key.channel();
          logger.info("Client socket channel to " + getHostPortString(clientSocketChannel) + ": is connectable");

        } else if (key.isReadable()) {

          // a client channel is ready for reading.
          SocketChannel clientSocketChannel = (SocketChannel) key.channel();
          SMTPClientSession smtpClientSession = channelClientSessions.get(clientSocketChannel);

          String msg = readMessage(clientSocketChannel);

          logger.info("Message received from " + getHostPortString(clientSocketChannel) + ":");
          logger.info(msg);

          if (smtpClientSession.getClientState() == ClientState.DATASENT && null != msg) {
            // Client sent message -> persist

            // content of email received
            smtpClientSession.setMailContent(msg);

            try {
              // Try to save message
              smtpClientSession.persistMessage();
              smtpClientSession.setClientState(ClientState.MESSAGESENT);
              smtpClientSession.setResponse(ServerResponse.OK.getResponse());
            } catch (IOException e) {
              logger.warn("Could not persist message with id" + smtpClientSession.getTimeCreated());
              logger.warn("handleRequests() - exception ignored", e);
            }

          } else if (msg.startsWith(ClientRequest.HELP.getRequest())) {

            // HELP command received
            // smtpClientSession.setClientState(ClientState.HELPSENT);
            smtpClientSession.setResponse(ServerResponse.HELP.getResponse());

          } else if (msg.startsWith(ClientRequest.HELO.getRequest())) {

            // HELO command received
            smtpClientSession.setClientState(ClientState.SENTWELCOME);
            smtpClientSession.setResponse(ServerResponse.OK.getResponse());

          }

          else if (msg.startsWith(ClientRequest.MAIL_FROM.getRequest())) {

            // sender email address received
            try {
              String mailFrom = msg.substring(msg.indexOf(ClientRequest.MAIL_FROM.getRequest()) + ClientRequest.MAIL_FROM.getRequest().length());
              smtpClientSession.setMailFrom(mailFrom);
              smtpClientSession.setClientState(ClientState.MAILFROMRECEIVED);
              smtpClientSession.setResponse(ServerResponse.OK.getResponse());
            } catch (IndexOutOfBoundsException e) {
              logger.error("handleRequests()", e);
              smtpClientSession.setResponse(ServerResponse.TMP_ERROR.getResponse() + "Could not extract sender's mail address in MAIL FROM: command:"
                      + e.getMessage());
            }

          } else if (msg.startsWith(ClientRequest.RCPT_TO.getRequest())) {

            // recipient email address received
            try {
              String recipient = msg.substring(msg.indexOf(ClientRequest.RCPT_TO.getRequest()) + ClientRequest.RCPT_TO.getRequest().length());
              smtpClientSession.setRecipient(recipient);
              smtpClientSession.setClientState(ClientState.RCPTTORECEIVED);
              smtpClientSession.setResponse(ServerResponse.OK.getResponse());
            } catch (IndexOutOfBoundsException e) {
              logger.error("handleRequests()", e);

              smtpClientSession.setResponse(ServerResponse.TMP_ERROR.getResponse() + "Could not extract recipients mail address in MAIL FROM: command: "
                      + e.getMessage());
            }

          } else if (msg.startsWith(ClientRequest.DATA.getRequest())) {

            // DATA command received
            smtpClientSession.setClientState(ClientState.DATASENT);
            smtpClientSession.setResponse(ServerResponse.START_MAIL_INPUT.getResponse());

          } else if (msg.startsWith(ClientRequest.QUIT.getRequest())) {

            // QUIT command received
            smtpClientSession.setClientState(ClientState.QUITSENT);
            smtpClientSession.setResponse(ServerResponse.CLOSING_CHANNEL.getResponse());

          }

        } else if (key.isWritable()) {

          // a client channel is ready for writing
          SocketChannel clientSocketChannel = (SocketChannel) key.channel();
          logger.debug("Client socket channel " + getHostPortString(clientSocketChannel) + " is ready for writing.");

          if (null != channelClientSessions.get(clientSocketChannel)) {
            SMTPClientSession smtpClientSession = channelClientSessions.get(clientSocketChannel);
            String message = smtpClientSession.getResponse();

            if (null != message) {
              writeMessage(clientSocketChannel, message);
              smtpClientSession.setResponse(null);
            }

            // close the channel/connection if a QUIT command was sent by the
            // client before
            if (smtpClientSession.getClientState() == ClientState.QUITSENT) {
              channelClientSessions.remove(clientSocketChannel);
              closeChannel(clientSocketChannel);
            }

          } else {
            logger.info("No session stored for client closing connection" + getHostPortString(clientSocketChannel) + " . Closing connection");
            closeChannel(clientSocketChannel);
          }

        }

        keyIterator.remove();
      }
    }
  }

  /**
   * Close the given socket channel.
   * 
   * @param clientSocketChannel
   *          the client socket channel
   */
  private void closeChannel(SocketChannel clientSocketChannel) {
    try {
      clientSocketChannel.close();
      logger.info("Connection to client" + getHostPortString(clientSocketChannel) + " closed.");

    } catch (IOException e) {
      logger.warn("Connection to client" + getHostPortString(clientSocketChannel) + " could not be closed: ", e);

    }
  }

  /**
   * Constructs the string HOSTNAME_ADDRESS:PORT for a given socket channel.
   * 
   * @param socketChannel
   *          the socket channel
   * @return the host port string
   */
  public final static String getHostPortString(SocketChannel socketChannel) {

    StringBuilder stringBuilder = new StringBuilder();

    stringBuilder.append(socketChannel.socket().getInetAddress().getHostAddress());
    stringBuilder.append(":");
    stringBuilder.append(socketChannel.socket().getPort());

    return stringBuilder.toString();
  }

  /**
   * Read a message from the given client socket channel.
   * 
   * @param clientSocketChannel
   *          the client socket channel
   * @return the string
   */
  private String readMessage(SocketChannel clientSocketChannel) {
    String message = null;
    ByteBuffer byteBuffer = ByteBuffer.allocate(8192);

    try {
      clientSocketChannel.read(byteBuffer);
      byteBuffer.flip();
      byte bytes[] = new byte[byteBuffer.limit()];

      byteBuffer.get(bytes);
      message = new String(bytes, Charset.forName("US-ASCII"));

    } catch (IOException e) {
      logger.error("Error reading from " + getHostPortString(clientSocketChannel));

    }
    logger.info("Got message from " + getHostPortString(clientSocketChannel) + ": " + message);
    return message;

  }

  /**
   * Writes the given message to the client socket channel.
   * 
   * @param clientSocketChannel
   *          the client socket channel
   * @param message
   *          the message
   */
  private void writeMessage(SocketChannel clientSocketChannel, String message) {

    ByteBuffer byteBuffer;
    try {
      byteBuffer = ByteBuffer.wrap((message + "\r\n").getBytes(Charset.forName(US_ASCII)));
      clientSocketChannel.write(byteBuffer);
      byteBuffer.clear();

      logger.info("Message written to " + getHostPortString(clientSocketChannel) + ": " + message);
    } catch (UnsupportedEncodingException e) {
      logger.error("writeMessage()", e);

    } catch (IOException e) {
      logger.error("writeMessage()", e);

    }

  }
}
